''' SQLite Table tools
'''

import sqlite3

from sqlite_keeper.queryable import Queryable


class Table(Queryable):
    ''' Table class
    '''
    def __repr__(self) -> str:
        columns = ','.join([c.name for c in self.columns])
        return f"<Table {self.name}({columns})>"

    @property
    def count(self) -> int:
        "Count of the rows in this table"
        return list(self.conn.query(f'SELECT count(*) FROM [{self.name}]'))[0][0]

    def trancate(self):
        ''' Trancate the table
        '''
        self.conn.execute(f'DELETE FROM [{self.name}]')

    def drop(self, ignore: bool = False):
        "Drop this table. ``ignore=True`` means errors will be ignored."
        try:
            self.conn.execute(f'DROP TABLE [{self.name}]')
        except sqlite3.OperationalError:
            if not ignore:
                raise
