
from typing import Any, Optional, Union
from typing import List, Dict, Tuple
from typing import Generator, Iterable

from collections import namedtuple

from sqlite_keeper.connection import Connection


Column = namedtuple(
            "Column", 
            ("cid", "name", "type", "not_null", "default_value", "is_pk")
)

class Queryable:

    def __init__(self, conn: Connection, name:str):
        self.conn = conn
        self.name = name

    @property
    def columns(self) -> List["Column"]:
        ''' List of `Columns <reference_db_other_column>` representing 
            the columns in this table or view.
        '''
        rows = self.conn.execute("PRAGMA table_info([{}])".format(self.name)).fetchall()
        return [Column(*row) for row in rows ]
        
    @property
    def rows(self) -> Generator[dict, None, None]:
        "Iterate over every dictionaries for each row in this table or view."
        return self.rows_where()

    def rows_where(self, where: str = None,
                    where_args: Optional[Union[Iterable, dict]] = None,
                    order_by: str = None,
                    select: str = "*",
                    limit: int = None,
                    offset: int = None) -> Generator[dict, None, None]:
        ''' Iterate over every row in this table or view that matches the specified where clause.

            - ``where`` - a SQL fragment to use as a ``WHERE`` clause, for example ``age > ?`` or ``age > :age``.
            - ``where_args`` - a list of arguments (if using ``?``) or a dictionary (if using ``:age``).
            - ``order_by`` - optional column or fragment of SQL to order by.
            - ``select`` - optional comma-separated list of columns to select.
            - ``limit`` - optional integer number of rows to limit to.
            - ``offset`` - optional integer for SQL offset.

            Returns each row as a dictionary. See :ref:`python_api_rows` for more details.
        '''
        sql = f"SELECT {select} FROM [{self.name}]"
        
        if where is not None:
            sql += " WHERE " + where
        if order_by is not None:
            sql += " ORDER BY " + order_by
        if limit is not None:
            sql += " LIMIT {}".format(limit)
        if offset is not None:
            sql += " OFFSET {}".format(offset)
        
        for row in self.conn.query(sql, where_args or [], result_as_dict=False):
            yield row

