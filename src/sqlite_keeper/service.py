
import csv
import json
import sqlite3

from sqlite_keeper.connection import Connection


class Service:

    def __init__(self, conn: Connection=None):
        ''' Init services
        '''
        assert conn, 'The connection cannot be None'
        self.conn = conn

    @property
    def versions(self):
        ''' returns sqlite version
        '''
        versions = {
            'Runtime SQLite library': sqlite3.sqlite_version,
            'Python SQLite module version': sqlite3.version,
        } 
        return versions

    def vacuum(self):
        ''' Run a SQLite ``VACUUM`` against the database.
        '''
        self.conn.execute("VACUUM;")

    def analyze(self, name:str=None):
        ''' Run ``ANALYZE`` against the entire database or a named table or index.'''
        sql = "ANALYZE"
        if name is not None:
            sql += f" [{name}]"
        self.conn.execute(sql)

    @property
    def journal_mode(self) -> str:
        "Get current ``journal_mode`` of this database."
        return self.conn.execute("PRAGMA journal_mode;").fetchone()[0]

    def enable_wal(self):
        "Set ``journal_mode`` to ``'wal'`` to enable Write-Ahead Log mode."
        if self.journal_mode != "wal":
            self.conn.execute("PRAGMA journal_mode=wal;")

    def disable_wal(self):
        "Set ``journal_mode`` back to ``'delete'`` to disable Write-Ahead Log mode."
        if self.journal_mode != "delete":
            self.conn.execute("PRAGMA journal_mode=delete;")

    # def rows_from_file(self, fp: BinaryIO, 
    #                 format: Optional[Format] = None,
    #                 dialect: Optional[Type[csv.Dialect]] = None,
    #                 encoding: Optional[str] = None) -> Tuple[Iterable[dict], Format]:

    #     ''' imports data from file
    #     '''    
    #     # Detect the format, then call this recursively
    #     buffered = io.BufferedReader(cast(io.RawIOBase, fp), buffer_size=4096)
    #     first_bytes = buffered.peek(2048).strip()
    #     if first_bytes.startswith(b"[") or first_bytes.startswith(b"{"):
    #         # TODO: Detect newline-JSON
    #         return rows_from_file(buffered, format=Format.JSON)
    #     else:
    #         dialect = csv.Sniffer().sniff(
    #             first_bytes.decode(encoding or "utf-8-sig", "ignore")
    #         )
    #         return rows_from_file(
    #             buffered, format=Format.CSV, dialect=dialect, encoding=encoding
    #         )
