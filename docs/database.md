# Database

Create data.db file, or open existing:
```python
db = Database("data.db")
```

Create an in-memory database:
```python
db = Database(memory=True)
```
