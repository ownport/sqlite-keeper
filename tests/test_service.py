
import re

from sqlite_keeper.service import Service
from sqlite_keeper.connection import Connection


def test_service_sqlite_version():
    ''' test service: getting sqlite version
    '''
    RE_VERSION = re.compile(r'\d+\.\d+\.\d+')
    versions = Service(Connection(':memory:')).versions
    assert RE_VERSION.match(versions['Runtime SQLite library']), \
        'Incorrect Runtime SQLite library version format'
    assert RE_VERSION.match(versions['Python SQLite module version']), \
        'Incorrect Python SQLite module version'
