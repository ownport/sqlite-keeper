
from sqlite_keeper.db import Database
from sqlite_keeper.connection import Connection

def test_db_init():
    ''' test init database
    '''
    assert Database(":memory:"), 'No database initiated'

def test_db_attach(tmp_path):
    ''' test db attach
    '''
    db1_path = tmp_path / 'db1.sqlite'
    db2_path = tmp_path / 'db2.sqlite'

    db = Database(db1_path)
    assert isinstance(db, Database), 'Cannot initiate database'

    db.attach('attached', db2_path)

def test_db_list_tables():
    ''' test db list of tables
    '''
    db = Database(':memory:')
    assert db.tables == [], 'Cannot get the list of tables'

    sql = 'CREATE TABLE kv1 (k, v)'
    db.conn.execute(sql)
    assert db.tables == [('kv1', sql)], 'Incorrect the list of tables'

def test_db_list_views():
    ''' test db list of views
    '''
    db = Database(':memory:')
    assert db.views == [], 'Cannot get the list of views'

    sql_create_table = 'CREATE TABLE kv (k, v);'
    sql_create_view = 'CREATE VIEW kvw AS SELECT k FROM kv;'
    db.conn.execute_script(''.join((sql_create_table, sql_create_view)))
    
    assert db.tables == [('kv', sql_create_table[:-1])], \
        'Incorrect the list of tables'
    assert db.views == [('kvw', sql_create_view[:-1])], \
        'Incorrect the list of views'

def test_db_list_triggers():
    ''' test db list of triggers
    '''
    db = Database(':memory:')
    assert db.triggers == [], 'Cannot get the list of triggers'

    sql_create_table = 'CREATE TABLE kv (k, v);'
    sql_create_trigger = '''
        CREATE TRIGGER add_more_record 
            AFTER INSERT ON kv 
        BEGIN 
            INSERT INTO kv (k,v) VALUES (k,v);
        END;
    '''
    db.conn.execute_script(''.join((sql_create_table, sql_create_trigger)))

    assert db.tables == [('kv', sql_create_table[:-1])], \
        'Incorrect the list of tables'
    assert db.triggers == [('add_more_record', 'kv', sql_create_trigger.strip()[:-1])], \
        'Incorrect the list of triggers'

def test_db_list_indexes():
    ''' test db list of indexes
    '''
    db = Database(':memory:')
    assert db.indexes == [], 'Cannot get the list of indexes'

    sql_create_table = 'CREATE TABLE kv (k, v);'
    sql_create_index = 'CREATE UNIQUE INDEX kv_unique_idx ON kv(k);'
    db.conn.execute_script(''.join((sql_create_table, sql_create_index)))

    assert db.tables == [('kv', sql_create_table[:-1])], \
        'Incorrect the list of tables'
    assert db.indexes == [('kv_unique_idx', 'kv', sql_create_index[:-1])], \
        'Incorrect the list of indexes'

def test_db_schema():
    ''' test db schema
    '''
    db = Database(':memory:')
    assert db.schema == [], 'Cannot get the database schema'

    sql_create_table = 'CREATE TABLE kv (k, v);'
    sql_create_index = 'CREATE UNIQUE INDEX kv_unique_idx ON kv(k);'
    db.conn.execute_script(''.join((sql_create_table, sql_create_index)))

    assert db.schema == [sql_create_table, sql_create_index], \
        'Incorrect database schema'

def test_db_connect_to_table():
    ''' test db connect to table
    '''
    db = Database(':memory:')
    db.conn.execute('CREATE TABLE kv (k STRING, v STRING)')
    db.conn.commit()

    assert str(db['kv']) == '<Table kv(k,v)>', 'Cannot connect to table'

def test_db_connect_to_view():
    ''' test db connect to schema
    '''
    db = Database(':memory:')
    db.conn.execute_script('''
        CREATE TABLE kv (k STRING, v STRING);
        CREATE VIEW kvv AS SELECT * FROM kv;
    ''')
    db.conn.commit()

    assert str(db['kvv']) == '<View kvv(k,v)>', 'Cannot connect to table'
