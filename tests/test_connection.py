
import sqlite3

from sqlite_keeper.connection import Connection

# ==============================================================
#  get_connection
# 
def test_get_connection_memory():
    ''' test get connection memory
    '''
    conn = Connection(':memory:')
    assert isinstance(conn, Connection), \
            'Failed to get Connection instance'
    assert isinstance(conn.conn, sqlite3.Connection), \
            'Failed to get sqlite connection'

def test_get_connection_reuse_sqlite_connection():
    ''' test get connection, reuse sqlite connection
    '''
    conn = Connection(':memory:')
    assert isinstance(conn, Connection), \
            'Failed to get Connection instance'
    assert isinstance(conn.conn, sqlite3.Connection), \
            'Failed to get sqlite connection'

    conn = Connection(conn.conn)
    assert isinstance(conn.conn, sqlite3.Connection), \
            'Failed to get sqlite connection with re-use sqlite connection'

def test_get_connection_filepath(tmp_path):
    ''' test get connection, filepath
    '''
    conn = Connection(tmp_path / 'db.sqlite')
    assert isinstance(conn, Connection), \
            'Failed to get Connection instance'
    assert isinstance(conn.conn, sqlite3.Connection), \
            'Failed to get sqlite connection'

def test_get_connection_filepath_with_recreate(tmp_path):
    ''' test get connection, filepath with re-create
    '''
    path = tmp_path / 'db.sqlite'
    conn = Connection(path)
    assert isinstance(conn, Connection), \
            'Failed to get Connection instance'
    assert isinstance(conn.conn, sqlite3.Connection), \
            'Failed to get sqlite connection'
    conn.commit()

    conn = Connection(path, recreate=True)
    assert isinstance(conn, Connection), \
            'Failed to get Connection instance'
    assert isinstance(conn.conn, sqlite3.Connection), \
            'Failed to get sqlite connection with re-create'

# ==============================================================
#  execute()
# 
def test_execute():
    ''' test execute method
    '''
    conn = Connection(':memory:')
    assert isinstance(
                conn.execute(
                    'CREATE TABLE t1 (k, v)'
                ), sqlite3.Cursor), \
            'Cannot create table'
    assert isinstance(
                conn.execute(
                    'INSERT INTO t1 VALUES (?, ?)', parameters=('k1', 'v1')
                ), sqlite3.Cursor), \
            'Cannot insert row by list of parameters'
    assert isinstance(
                conn.execute(
                    'INSERT INTO t1 VALUES (:k, :v)', parameters={'k': 'k2', 'v': 'v2'}
                ), sqlite3.Cursor), \
            'Cannot insert row with parameters as dictionary'
    assert list(conn.query('SELECT k , v FROM t1')) == [
                ('k1', 'v1'),
                ('k2', 'v2'),
            ], 'Cannot query table or results mismatch'


# ==============================================================
#  execute_script()
# 
def test_execute_script():
    ''' test execute_script() method
    '''
    conn = Connection(':memory:')
    assert isinstance(
                conn.execute_script(
                    "CREATE TABLE t1 (k, v);INSERT INTO t1 VALUES ('k1', 'v1');"
                ),sqlite3.Cursor), \
            'Failed to execute the script'
    assert list(conn.query('SELECT k , v FROM t1')) == [('k1', 'v1')], \
        'Cannot query table or results mismatch'
    

# ==============================================================
#  query()
# 
def test_query():
    ''' test execute_script() method
    '''
    conn = Connection(':memory:')
    assert isinstance(
                conn.execute_script(
                    "CREATE TABLE t1 (k, v);INSERT INTO t1 VALUES ('k1', 'v1');"
                ), sqlite3.Cursor), \
            'Failed to execute the script'
    assert list(conn.query('SELECT k , v FROM t1', result_as_dict=True)) == [{'k': 'k1', 'v': 'v1'}], \
        'Cannot query table or results mismatch'
    assert list(conn.query('SELECT k , v FROM t1')) == [('k1', 'v1')], \
        'Cannot query table or results mismatch'

