''' tests for sqlite_tools.formats.csvfile module
'''
import pathlib
import pytest

from sqlite_keeper.formats.csvfile import CSVFile
from sqlite_keeper.formats.csvfile import TSVFile

# ======================================================================
#   CSVFile
#
def test_formats_csv_load_with_header():
    ''' test formats CSV file load, with header
    '''
    data = CSVFile.load(pathlib.Path('tests/resources/data-with-header.csv'),
                        header=True)
    data = list(data)
    assert len(data) == 5, 'Incorrect records number'
    assert data[0] == {'First name': 'Rachel',
                            'Identifier': '9012',
                            'Last name': 'Booker',
                            'Username': 'booker12'
                        }, 'Incorrect first record'

def test_formats_csv_load_with_no_header():
    ''' test formats CSV file load, w/o header
    '''
    data = CSVFile.load(pathlib.Path('tests/resources/data-no-header.csv'),
                        header=False)
    data = list(data)
    assert len(data) == 5, 'Incorrect records number'
    assert data[0] == ['booker12', '9012', 'Rachel', 'Booker'], \
        'Incorrect first record'


def test_formats_csv_load_file_not_exist():
    ''' test formats CSV file load, file does not exist
    '''
    with pytest.raises(IOError):
        list(CSVFile.load(pathlib.Path('tests/resources/no-file.csv'),
                            header=False))


# ======================================================================
#   TSVFile
#
def test_formats_tsv_load_with_header():
    ''' test formats TSV file load, with header
    '''
    data = TSVFile.load(pathlib.Path('tests/resources/data-with-header.tsv'),
                        header=True)
    data = list(data)
    assert len(data) == 5, 'Incorrect records number'
    assert data[0] == {'First name': 'Rachel',
                            'Identifier': '9012',
                            'Last name': 'Booker',
                            'Username': 'booker12'
                        }, 'Incorrect first record'

def test_formats_tsv_load_with_no_header():
    ''' test formats CSV file load, w/o header
    '''
    data = TSVFile.load(pathlib.Path('tests/resources/data-no-header.tsv'),
                        header=False)
    data = list(data)
    assert len(data) == 5, 'Incorrect records number'
    assert data[0] == ['booker12', '9012', 'Rachel', 'Booker'], \
        'Incorrect first record'
