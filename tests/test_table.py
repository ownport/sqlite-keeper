''' The tests for sqlite_tools.table module
'''
import sqlite3
import pytest

from sqlite_keeper.db import Database
from sqlite_keeper.table import Table


SQL_SCRIPT = '''
    CREATE TABLE kv (k STRING, v STRING);
    INSERT INTO kv (k, v) VALUES ("k1", "v1");
    INSERT INTO kv (k, v) VALUES ("k2", "v2");
    INSERT INTO kv (k, v) VALUES ("k3", "v3");
    INSERT INTO kv (k, v) VALUES ("k4", "v4");
    INSERT INTO kv (k, v) VALUES ("k5", "v5");
'''

def test_table_common_methods():
    ''' test table, common methods
    '''
    _db = Database(':memory:')
    _db.conn.execute_script(SQL_SCRIPT)
    _db.conn.commit()

    assert isinstance(_db['kv'], Table), 'Incorrect table type'
    assert str(_db['kv']) == '<Table kv(k,v)>', 'Incorrent table string representation'
    assert _db['kv'].count == 5, 'Incorrect rows number'

    _db['kv'].drop()

def test_table_unknown_type():
    ''' test table unknown type
    '''
    _db = Database(':memory:')
    with pytest.raises(RuntimeError):
        table = _db['kv']
        assert table.name == 'kv', 'Incorrect table name'

def test_table_trancate():
    ''' test trancate table
    '''
    _db = Database(':memory:')
    _db.conn.execute_script(SQL_SCRIPT)
    _db.conn.commit()
    assert _db['kv'].count == 5, 'Incorrect records number'

    _db['kv'].trancate()
    assert _db['kv'].count == 0, 'Incorrect records number'

def test_table_drop():
    ''' test drop table
    '''
    _db = Database(':memory:')
    _db.conn.execute_script(SQL_SCRIPT)
    _db.conn.commit()

    kv_table = _db['kv']
    kv_table.drop()
    kv_table.drop(ignore=True)

    with pytest.raises(sqlite3.OperationalError):
        kv_table.drop()
