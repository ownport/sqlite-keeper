
import pytest

from sqlite_keeper.db import Database

@pytest.fixture
def fresh_db():
    return Database(':memory:')
