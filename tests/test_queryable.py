
from sqlite_keeper.connection import Connection
from sqlite_keeper.queryable import Queryable
from sqlite_keeper.queryable import Column

def test_queryable_init():
    ''' test queryable init
    '''
    conn = Connection(':memory:')
    conn.execute('CREATE TABLE kv(k STRING, v STRING);')

    q = Queryable(conn, 'kv')

    assert isinstance(q, Queryable), 'Cannot create Queryable instance'
    columns = q.columns
    assert columns == [
        Column(cid=0, name='k', type='STRING', not_null=0, default_value=None, is_pk=0), 
        Column(cid=1, name='v', type='STRING', not_null=0, default_value=None, is_pk=0)
    ], 'Columns mismatch'

def test_queryable_init():
    ''' test queryable init
    '''
    data = [('k1', 'v1'), ('k2', 'v2'), ('k3', 'v3')]
    conn = Connection(':memory:')
    conn.execute('CREATE TABLE kv(k STRING, v STRING);')
    conn.execute_many('INSERT INTO kv (k,v) VALUES (?, ?)', data)
    conn.commit()

    q = Queryable(conn, 'kv')
    results = list(q.rows)
    assert results == data, 'Rows mismatch'

def test_queryable_init():
    ''' test queryable init
    '''
    data = [('k1', 'v1'), ('k2', 'v2'), ('k3', 'v3')]
    conn = Connection(':memory:')
    conn.execute('CREATE TABLE kv(k STRING, v STRING);')
    conn.execute_many('INSERT INTO kv (k,v) VALUES (?, ?)', data)
    conn.commit()

    q = Queryable(conn, 'kv')
    results = list(q.rows_where(where='k="k1"', order_by='k', limit=1, offset=0))
    assert results == [('k1', 'v1')], 'Rows mismatch'


